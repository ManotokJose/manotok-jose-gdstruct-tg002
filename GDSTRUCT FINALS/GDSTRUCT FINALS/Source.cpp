#include <iostream>
#include <string>
#include "Unordered.h"
#include "Stack.h"
#include <time.h>

using namespace std;

void main()
{
	int size;

	UnorderedArray<int> unordered(1);
	Stack<int> stack(1);

	while (true)
	{
		int playerInput;
		cout << "What will you do to the array?" << endl;
		cout << "[1] Push " << endl;
		cout << "[2] Pop" << endl;
		cout << "[3] Top" << endl;
		cin >> playerInput;

		switch (playerInput)
		{
		case 1:
			cout << "What number do you want to push? " << endl;
			cin >> playerInput;
			unordered.push(playerInput);
			stack.push(playerInput);
			break;
		case 2:
			unordered.pop();
			stack.pop();
			break;
		case 3:
			cout << "Unordered Top :";
			cout << unordered.top() << endl;
			cout << "Stack Top :";
			cout << stack.top() << endl;
		}

		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";

		unordered.printArray();
		cout << endl;

		cout << "\nStack array: " << endl;
		cout << "Stack: ";

		stack.printArray();
		cout << endl;
	}

	system("pause");
	cout << endl << endl;
}