#include <iostream>
#include <string>
#include "OrderedArray.h"
#include "UnorderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));

	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "\nGenerated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";

	cout << endl << endl;
	int playerInput;
	cout << "What do you want to do?" << endl;
	cout << "1 - Remove element at index" << endl;
	cout << "2 - Search for element" << endl;
	cout << "3 - Expand and generate random values" << endl;
	cin >> playerInput;

	switch (playerInput)
	{
		int input;
		int result;
	case 1:
		cout << "\n\nEnter index to remove: ";
		cin >> input;
		cout << endl;

		cout << "Element removed: " << input << endl;
		unordered.remove(input);
		ordered.remove(input);
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";
		break;

	case 2:
		cout << "\n\nEnter number to search: ";
		cin >> input;
		cout << endl;

		cout << "Unordered Array(Linear Search):\n";
		result = unordered.linearSearch(input);
		if (result >= 0)
			cout << input << " was found at index " << result << ".\n";
		else
			cout << input << " not found." << endl;

		cout << "Ordered Array(Binary Search):\n";
		result = ordered.binarySearch(input);
		if (result >= 0)
			cout << input << " was found at index " << result << ".\n";
		else
			cout << input << " not found." << endl;
		system("pause");
		break;

	case 3:
		int number;

		cout << "Input size of expansion :";
		cin >> number;
		for (int i = 0; i < number; i++)
		{
			int rng = rand() % 101;
			unordered.push(rng);
			ordered.push(rng);
		}

		cout << endl;
		cout << "Arrays have been expanded: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";
		break;
	}


}