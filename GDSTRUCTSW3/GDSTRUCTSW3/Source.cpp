#include <string>
#include <iostream>

using namespace std;

void addNumber(int from, int sum = 0)
{

	if (from == 0)
	{
		cout << sum;
		return;
	}
	sum = sum + from % 10;
	from = from / 10;

	addNumber(from, sum);
}

void fibonacciNumbers(int term, int first = 0, int last = 1)
{
	if (term <= 0)
		return;

	if (first == 0)
		cout << first << " " << last;

	cout << " ";
	cout << first + last;
	fibonacciNumbers(term - 1, last, first + last);
}

void findIfPrimeNumber(int input, int number = 9)
{
	if (number <= 1)
	{
		cout << input << " is a prime number";
		return;
	}

	if (input % number == 0)
	{
		cout << input << " is divisible by " << number;
		return;
	}

	findIfPrimeNumber(input, number - 1);
}

void main()
{
	addNumber(9999);
	cout << endl;
	fibonacciNumbers(10);
	cout << endl;
	findIfPrimeNumber(600);
	cout << endl;
	system("pause");
}